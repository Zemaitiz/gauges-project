# FP-The Gauge Project
Software Development Academy. Final Project. The Gauge Project

Using this app you can record readings of gauges (hot, cold water, electricity etc.). Some more features include:
- Adding/reading individual gauge readings
- Listing all gauge readings (by gauge type and/or date of reading)
- Recording of all month's gauge readings in one go (group reading)
- Finding group readings by date (a.k.a. finding readings of one month)
- Sending an email containing the readings to the landlord


Application uses Java 8, Spring, Angular, MySQL, REST API endpoints.
