INSERT INTO gauge(code, type_description, type_description_lt, units) VALUES('kh', 'Kitchen hot water', 'Karštas vanduo (virtuvė)', 'm^3');
INSERT INTO gauge(code, type_description, type_description_lt, units) VALUES('kc', 'Kitchen cold water', 'Šaltas vanduo (virtuvė)', 'm^3');
INSERT INTO gauge(code, type_description, type_description_lt, units) VALUES('bh', 'Bathroom hot water', 'Karštas vanduo (tualetas)', 'm^3');
INSERT INTO gauge(code, type_description, type_description_lt, units) VALUES('bc', 'Bathroom cold water', 'Šaltas vanduo (tualetas)', 'm^3');
INSERT INTO gauge(code, type_description, type_description_lt, units) VALUES('ed', 'Daytime energy', 'Elektra (dieninis)', 'kWh');
INSERT INTO gauge(code, type_description, type_description_lt, units) VALUES('en', 'Nighttime energy', 'Elektra (naktinis)', 'kWh');
