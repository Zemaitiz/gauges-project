DROP TABLE IF EXISTS gauge;
CREATE TABLE gauge (
    `id` LONG NOT NULL AUTO_INCREMENT,
    `code` VARCHAR(245),
    `type_description` VARCHAR(245),
    `type_description_lt` VARCHAR(245),
    `units` VARCHAR(245),
    PRIMARY KEY (`id`)
);

