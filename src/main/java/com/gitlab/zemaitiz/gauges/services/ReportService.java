package com.gitlab.zemaitiz.gauges.services;

import com.gitlab.zemaitiz.gauges.model.GaugeData;
import com.gitlab.zemaitiz.gauges.model.GroupReading;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class ReportService {
    public String generateReport(GroupReading groupReading) {
        String report = groupReading.getReadingDate().toString().substring(
                0, groupReading.getReadingDate().toString().length() - 3)
                + " mėnesio skaitiklių rodmenys:" + "\n";

       Set<GaugeData> gaugeDataSet = groupReading.getGaugeDataSet();

        for (GaugeData gaugeData : gaugeDataSet) {
            report += gaugeData.toString() + "\n";
        }
        report += "----------------------------------" + "\n" + " Kitos paslaugos " + groupReading.getOtherServices() + " EUR";
        return report;
    }

}
