package com.gitlab.zemaitiz.gauges.services;

import com.gitlab.zemaitiz.gauges.model.Gauge;
import com.gitlab.zemaitiz.gauges.repository.GaugeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class GaugeService {
    @Autowired
    private GaugeRepository gaugeRepository;



    //READ by id
    public Optional<Gauge> findGaugeById(Long gaugeId) {
        return gaugeRepository.findById(gaugeId);
    }

    //READ all
    public List<Gauge> findAllGauges() {
        return (List<Gauge>) gaugeRepository.findAll();
    }


    //Update by id, only for admin
    public void  updateGauge(Long gaugeId, Gauge gauge) {
        Optional<Gauge> gaugeToUpdate = findGaugeById(gaugeId);
        gaugeToUpdate.get().setCode(gauge.getCode());
        gaugeToUpdate.get().setTypeDescription(gauge.getTypeDescription());
        gaugeToUpdate.get().setUnits(gauge.getUnits());
        gaugeRepository.save(gaugeToUpdate.get());
    }

    //DELETE by id, only for admin

    public void deleteGaugeById(Long gaugeId) {
       gaugeRepository.delete(findGaugeById(gaugeId).get());
    }
}
