package com.gitlab.zemaitiz.gauges.controller;

import com.gitlab.zemaitiz.gauges.exceptions.RecordNotFoundException;
import com.gitlab.zemaitiz.gauges.model.GaugeData;
import com.gitlab.zemaitiz.gauges.model.GaugeDataDTO;
import com.gitlab.zemaitiz.gauges.model.GroupReading;
import com.gitlab.zemaitiz.gauges.model.GroupReadingDataDTO;
import com.gitlab.zemaitiz.gauges.services.EmailServiceImpl;
import com.gitlab.zemaitiz.gauges.services.GroupReadingService;
import com.gitlab.zemaitiz.gauges.services.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/groupreading")
public class GroupReadingController
        //extends ApiRestController
{

    @Autowired
    private GroupReadingService groupReadingService;

    @Autowired
    private ReportService reportService;

    @Autowired
    private EmailServiceImpl emailService;



    //Populate db with pseudo random group Readings
    @PostMapping("/populate")
public void populate() {
    groupReadingService.populateWithPseudoRandom();
}

    //CREATE new group reading
    @PostMapping("/new")
    public void addGroupReading(@RequestBody GroupReadingDataDTO groupReadingDataDTO) {
        groupReadingService.saveGroupReading(groupReadingDataDTO);
    }


    //UPDATE with a list of missing GaugeData readings and assign it to GroupReading's set

    @PutMapping("{groupReadingId}")
    public void updateGroupReading(@PathVariable Long groupReadingId, @RequestBody List<GaugeDataDTO> gDto) {
        groupReadingService.updateGroupReading(groupReadingId, gDto);
    }

    //READ set of gaugedata of one group reading by id

    @GetMapping("/{groupReadingId}")
    private Set<GaugeData> findGaugeDataSetByGroupReadingId(@PathVariable Long groupReadingId) {
        return groupReadingService.findAllByGroupReadingId(groupReadingId);
    }

    //READ GaugeData set by date
    @GetMapping("/bydate")
    public Set<GaugeData> findGroupReadingGaugeDataSetByDate(@RequestParam int year, @RequestParam int month) {
        return groupReadingService.findGroupReadingGaugeDataSetByDate(year, month);
    }


    //DELETE GROUP READING BY ID

    @DeleteMapping("/{groupReadingId}")
    private void deleteGroupReadingById(@PathVariable Long groupReadingId) {
        groupReadingService.deleteGroupReadingById(groupReadingId);
    }

    //READ gaugedatasets of all groupreadings

    @GetMapping("/all")
    private List<Set<GaugeData>> findAllGroupReadingsGaugeDataSets() {
        return groupReadingService.findAllGroupReadingsGaugeDataSets();
    }


    @GetMapping("/{groupReadingId}/report")
    private String generateReport(@PathVariable Long groupReadingId) {
       return reportService.generateReport(groupReadingService.findById(groupReadingId));
    }

    @PostMapping("/{groupReadingId}/e-report")
    private void sendReport(@PathVariable Long groupReadingId) {
        emailService.sendEmail(reportService.generateReport(groupReadingService.findById(groupReadingId)));
    }

    @GetMapping("/incomplete")
    private List<GroupReading> findIncompleteGroupReadings() {
        return groupReadingService.findIncompleteGroupReadings();
    }
}
