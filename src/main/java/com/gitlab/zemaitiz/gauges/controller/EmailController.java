package com.gitlab.zemaitiz.gauges.controller;

import com.gitlab.zemaitiz.gauges.repository.GroupReadingRepository;
import com.gitlab.zemaitiz.gauges.services.EmailServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmailController {
    @Autowired
    private EmailServiceImpl emailService;

    @Autowired
    private GroupReadingRepository groupReadingRepository;

    @PostMapping("/report/{groupReadingId}")
    private void sendEmail(@PathVariable Long groupReadingId) {

        emailService.sendEmail(groupReadingRepository.findById(groupReadingId).toString());
    }
}
