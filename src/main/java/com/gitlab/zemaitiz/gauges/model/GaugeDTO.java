package com.gitlab.zemaitiz.gauges.model;

public class GaugeDTO {

    private String code;

    public GaugeDTO(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
