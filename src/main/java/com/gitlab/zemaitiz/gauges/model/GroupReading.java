package com.gitlab.zemaitiz.gauges.model;



import com.sun.istack.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;


import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.SortedSet;

@Entity
@Table(name = "group_reading")
public class GroupReading {
    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "groupReading")
    @EqualsAndHashCode.Exclude
    @OrderBy
    private Set<GaugeData> gaugeDataSet = new LinkedHashSet<>();
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate readingDate = LocalDate.now();  //Nezinau kodel, bet tik taip nemeta NullPointerException
    private BigDecimal otherServices;
    private boolean isComplete;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<GaugeData> getGaugeDataSet() {
        return gaugeDataSet;
    }

    public void setGaugeDataSet(Set<GaugeData> gaugeDataSet) {
        this.gaugeDataSet = gaugeDataSet;
    }

    public LocalDate getReadingDate() {
        return readingDate;
    }

    public void setReadingDate(LocalDate readingDate) {
        this.readingDate = readingDate;
    }

    public BigDecimal getOtherServices() {
        return otherServices;
    }

    public void setOtherServices(BigDecimal otherServices) {
        this.otherServices = otherServices;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }

    @Override
    public String toString() {
        return "GroupReading{" +
                "id=" + id +
                ", gaugeDataSet=" + gaugeDataSet +
                ", readingDate=" + readingDate +
                '}';
    }
}
